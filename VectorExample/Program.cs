﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VectorClasses;


namespace VectorExample
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            IVectorADT<String> v 
                = new ArrayBasedVectorDS<String>(3);

            v.InsertElementAtRank(0, "A");
            v.InsertElementAtRank(1, "B");
            v.InsertElementAtRank(2, "D");
            v.InsertElementAtRank(3, "E");

            // example on whiteboard
            v.InsertElementAtRank(2, "C");

            v.InsertElementAtRank(1, "Element to Remove");

            Console.WriteLine(
                    v.RemoveElementAtRank(1)
                );
            */

            IQueueADT<String> queue = new VectorClasses.Queue<String>();
            IStackADT<String> stack = new VectorClasses.Stack<String>();

            queue.Enqueue("A");
            queue.Enqueue("B");
            queue.Enqueue("C");
            queue.Enqueue("D");
            queue.Enqueue("E");

            stack.Push("A");
            stack.Push("B");
            stack.Push("C");
            stack.Push("D");
            stack.Push("E");

            List<String> output = new List<String>(
                    queue.Size()
                );

            while (queue.Size() > 0)
            {
                output.Add(
                    queue.Dequeue()
                );
            }

            Console.WriteLine("Queue's output:");
            Console.WriteLine(
                String.Join(", ", output)
            );


            Console.WriteLine();
            Console.WriteLine();

            output = new List<String>(
                    stack.Size()
                );

            while (stack.Size() > 0)
            {
                output.Add(
                    stack.Pop()
                );
            }

            Console.WriteLine("Stack's output:");
            Console.WriteLine(
                String.Join(", ", output)
            );

            Console.ReadKey();
        }
    }
}
