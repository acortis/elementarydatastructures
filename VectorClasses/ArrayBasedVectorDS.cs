﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VectorClasses
{
    /// <summary>
    /// A class representing the ArrayBasedVector data structure.
    /// This is an implementation of the IVectorADT.
    /// The implementation is based on an array.
    /// </summary>
    public class ArrayBasedVectorDS<T> : IVectorADT<T>
    {
        private T[] V;
        private int size = 0;

        public ArrayBasedVectorDS(int capacity = 4)
        {
            V = new T[capacity];
        }

        public T this[int r]
        {
            get
            {
                return GetElementAtRank(r);
            }
            set
            {
                ReplaceElementAtRank(r, value);
            }
        }
                

        public T GetElementAtRank(int r)
        {
            #region Validation
            if (r < 0 || r >= Size())
            {
                throw new ArgumentOutOfRangeException("r", r, "The value of r must be between 0 and Size() - 1 (both inclusive)");
            }
            #endregion

            return V[r];
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="r"></param>
        /// <param name="element"></param>
        /// <remarks>
        /// Best Case: a constant number of operations.  This happens when there are 0 copies in an array of length n
        /// Worst Case: about n operations.  This happens when inserting at rank 0.  All n elements need to be copied
        /// </remarks>
        public void InsertElementAtRank(int r, T element)
        {

            #region Validation
            if (r < 0 || r >= Size() + 1)
            {
                throw new ArgumentOutOfRangeException("r", r, "The value of r must be between 0 and Size() (both inclusive)");
            }

            if (Size() >= V.Length)
            {
                // Array is full

                // Array "growth"
                int newSize = V.Length * 2; // ToDo choose a new size that is "efficient"

                T[] newArray = new T[newSize];

                // copy the items up to position r from old array to the new array
                for (int i = 0; i < r; i++)
                {
                    newArray[i] = V[i];
                }

                // add new element
                newArray[r] = element;

                // copy all the remaining elements, shifted by +1
                for (int i = r; i < V.Length; i++)
                {
                    newArray[i + 1] = V[i];
                }

                // set the new Array
                V = newArray;
                size++;

                // ready!
                return;
            }

            #endregion
            
            for (int i = Size() - 1; i >= r; i--)
            {
                V[i + 1] = V[i];
            }
            
            V[r] = element;
            size++;
        }

        public T RemoveElementAtRank(int r)
        {
            T removedElement = V[r];

            // remove old item... and close "space"
            for (int i = r + 1; i <= Size() - 1; i++)
            {
                V[i - 1] = V[i];
            }

            V[Size() - 1] = default(T);

            size--;
            return removedElement;

        }

        public T ReplaceElementAtRank(int r, T element)
        {
            T oldElement = V[r];
            V[r] = element;

            return oldElement;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="element"></param>
        /// <remarks>
        /// 0 copies are required.  This operation always takes a few operations, regardless of size (assuming no array growth).
        /// </remarks>
        public void Append(T element)
        {
            InsertElementAtRank(Size(), element); // require 0 copies...
        }

        public int Size()
        {
            return size;
        }
    }
}
