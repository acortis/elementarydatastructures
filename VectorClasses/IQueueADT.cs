﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VectorClasses
{
    public interface IQueueADT<T>
    {
        int Size();

        void Enqueue(T elem);

        T Dequeue();

        T Peek();
    }
}
