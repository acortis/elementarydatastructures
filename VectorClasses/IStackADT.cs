﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VectorClasses
{
    /// <summary>
    /// Represents a StackADT
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IStackADT<T>
    {
        /// <summary>
        /// The number of items on the stack
        /// </summary>
        /// <returns></returns>
        int Size();

        /// <summary>
        /// Push a new item to the top of the stack
        /// </summary>
        /// <param name="element"></param>
        void Push(T element);

        /// <summary>
        /// Remove the item from the top of the stack
        /// </summary>
        /// <returns></returns>
        T Pop();

        /// <summary>
        /// Returns the item from the top of the stack, WITHOUT removing it
        /// </summary>
        /// <returns></returns>
        T Peek();
    }
}
