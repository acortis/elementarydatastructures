﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VectorClasses
{
    public class Queue<T> : IQueueADT<T>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <remarks>
        /// The standard implementation of the ArrayBasedVector cannot allow for an efficient Queue.
        /// Instead, consider either: (i) linked lists; or (ii) a circular buffer
        /// </remarks>
        private ArrayBasedVectorDS<T> arrayBasedVector = new ArrayBasedVectorDS<T>();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// <remarks>
        /// Slow! Removing element at rank 0, requires shifting all the other n elements by 1 position!
        /// This takes about n operations, where n is the number of elements in the ArrayBasedVector (i.e., Size())
        /// </remarks>
        public T Dequeue()
        {
            return arrayBasedVector.RemoveElementAtRank(0);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="elem"></param>
        /// <remarks>
        /// This is still fast, since we are using the Append operation.
        /// This takes about 1 operation.
        /// </remarks>
        public void Enqueue(T elem)
        {
            arrayBasedVector.Append(elem);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// <remarks>
        /// This is still fast and requires about 1 operation, since we are not removing or moving anything!
        /// </remarks>
        public T Peek()
        {
            return arrayBasedVector.GetElementAtRank(0);
        }

        public int Size()
        {
            return arrayBasedVector.Size();
        }
    }
}
