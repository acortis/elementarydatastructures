﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VectorClasses
{
    /// <summary>
    /// The IVectorADT represents the Vector Abstract Data Type.
    /// This is an ordered sequence of elements of type T
    /// </summary>
    public interface IVectorADT<T>
    {
        /// <summary>
        /// Returns the number of elements in the Vector
        /// </summary>
        /// <returns>The number of elements in the Vector</returns>
        int Size();

        /// <summary>
        /// Returns the element found at rank r
        /// </summary>
        /// <param name="r">The rank to get the element from.  Must be between 0 and Size() - 1, both inclusive.</param>
        /// <returns>The element found at rank r</returns>
        T GetElementAtRank(int r);

        /// <summary>
        /// </summary>
        /// <param name="r"></param>
        /// <returns></returns>
        /// <remarks>
        /// Indexer, to faciliate the use of GetElementAtRank and ReplaceElementAtRank.
        /// </remarks>
        T this[int r] { get; set; }
        
        void InsertElementAtRank(int r, T element);

        T ReplaceElementAtRank(int r, T element);

        T RemoveElementAtRank(int r);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="element"></param>
        void Append(T element);
    }
}
