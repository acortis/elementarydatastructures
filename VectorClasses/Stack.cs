﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VectorClasses
{
    public class Stack<T> : IStackADT<T>
    {
        private ArrayBasedVectorDS<T> arrayBasedVector = new ArrayBasedVectorDS<T>();

        public T Peek()
        {
            return arrayBasedVector.GetElementAtRank(
                    arrayBasedVector.Size() - 1
                );
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// <remarks>
        /// Only requires about 1 operation
        /// </remarks>
        public T Pop()
        {
            return
                arrayBasedVector.RemoveElementAtRank(
                    arrayBasedVector.Size() - 1         // rank of the last element in the stack
                );
        }

        public void Push(T element)
        {
            // requires about 1 operation (except for cases where array growth is required)
            arrayBasedVector.Append(element);
        }

        public int Size()
        {
            return
                arrayBasedVector.Size();
        }
    }
}
